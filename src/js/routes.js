
import HomePage from '../pages/home.svelte';
import AboutPage from '../pages/about.svelte';
import AboutSubPage from '../pages/aboutsub.svelte';
import SettingsPage from '../pages/settings.svelte';
import NotFoundPage from '../pages/404.svelte';

const routes = [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/about/subpage/',
    component: AboutSubPage,
  },
  {
    path: '/settings/',
    component: SettingsPage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
]

export default routes;
